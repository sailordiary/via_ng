# VGG Image Annotator 2

VIA2 is a major upgrade to [VGG Image Annotator](https://gitlab.com/vgg/via/) with 
addition of the following features:
 * [Video Annotation](docs/README.md#video-annotation)
 * [Distributed Annotation](docs/README.md#distributed-annotation)
 * [Assistant](docs/README.md#assistant)
 * [User Interface](docs/README.md#user-interface)
 * [File List](docs/README.md#file-list)
 * [Import and Export](docs/README.md#import-and-export)
 * [Code Documentation](docs/README.md#code-documentation)

See the [docs/README.md](docs/README.md) for more details.

Abhishek Dutta  
17 June 2017
