
////////////////////////////////////////////////////////////////////////////////
//
// @file        _via_ctrl.js
// @description Implements Controller in the MVC architecture of VIA
// @author      Abhishek Dutta <adutta@robots.ox.ac.uk>
// @date        17 June 2017
//
////////////////////////////////////////////////////////////////////////////////

function _via_ctrl() {
  this.hook = {};
  this.hook.id = {REGION_ADDED: 'REGION_ADDED',
                  REGION_MOVED: 'REGION_MOVED',
                  REGION_RESIZED: 'REGION_RESIZED'
                  };

  this.hook.now = {};
}

function _via_point(x, y) {
  this.x = x;
  this.y = y;
}

///
/// initialization routines
///
_via_ctrl.prototype.init = function(via_model, via_view) {
  //console.log('Initializing _via_ctrl ...');
  this.m = via_model;
  this.v = via_view;

  this.init_defaults();
  this.init_view_panel();

  // add user input handlers
  this.v.layers.top.addEventListener('keydown', this.keydown_handler.bind(this), false);
  this.v.layers.top.addEventListener('keyup', this.keyup_handler.bind(this), false);

  this.v.set_state( this.v.state.IDLE );
  this.v.layers.top.focus();
}

_via_ctrl.prototype.init_defaults = function() {
  this.set_region_shape('rect');
}

///
/// hook maintainers
///
_via_ctrl.prototype.add_hook = function(hook_name, listener) {
  if ( this.hook.now.hasOwnProperty(hook_name) ) {
  } else {
    this.hook.now[hook_name] = [];
    this.hook.now[hook_name].push(listener);
  }
}

_via_ctrl.prototype.del_hook = function(hook_name, listener) {
  if ( this.hook.now[hook_name].includes(listener) ) {
    var index = this.hook.now[hook_name].indexOf(listener);
    this.hook.now[hook_name].splice(index, 1);
  }
}

_via_ctrl.prototype.clear_all_hooks = function(hook_name) {
  this.hook.now[hook_name] = [];
}

_via_ctrl.prototype.trigger_hook = function(hook_name, param) {
  return new Promise( function(ok_callback, err_callback) {
    if ( this.hook.now.hasOwnProperty(hook_name) ) {
      for ( var i=0; i<this.hook.now[hook_name].length; i++ ) {
        var target = this.hook.now[hook_name][i];
        i
        target.call(null, param);
      }
    }
  }.bind(this));
}

///
/// maintainers of the view panel
///
_via_ctrl.prototype.init_view_panel = function() {
  // ensure that all the layers fill up the parent container
  this.v.view_panel.style.width = '100%';
  this.v.view_panel.style.height = '100%';
  this.v.view_panel.width  = this.v.view_panel.offsetWidth;
  this.v.view_panel.height = this.v.view_panel.offsetHeight;

  this.v.layers = {};
  this.v.layers['filecontent'] = document.createElement('div');
  this.v.layers['filecontent'].setAttribute('id', 'filecontent');
  //this.v.layers['filecontent'].style.zIndex = "9971";

  this.v.layers['rshape'] = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  this.v.layers['rshape'].setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xlink', 'http://www.w3.org/2000/svg');
  this.v.layers['rshape'].setAttributeNS(null, 'id', 'rshape');
  //this.v.layers['rshape'].style.zIndex = "9972";

  this.v.layers['rattr'] = document.createElement('div');
  this.v.layers['rattr'].setAttribute('id', 'rattr');
  //this.v.layers['rattr'].style.zIndex = "9973";

  this.v.layers['zoom'] = document.createElement('div');
  this.v.layers['zoom'].setAttribute('id', 'zoom');
  //this.v.layers['zoom'].style.zIndex = "9974";

  this.v.layers['top'] = document.createElement('div');
  this.v.layers['top'].setAttribute('id', 'top');
  this.v.layers['top'].setAttribute('tabindex', '1');
  //this.v.layers['top'].style.zIndex = "9980";

  for ( var layer in this.v.layers ) {
    if ( this.v.layers.hasOwnProperty(layer) ) {
      this.v.view_panel.appendChild(this.v.layers[layer]);
    }
  }

  // attach mouse and touch event handlers
  this.v.layers['top'].addEventListener('mousemove', this.mousemove_handler.bind(this), false);
  this.v.layers['top'].addEventListener('mouseup'  , this.mouseup_handler.bind(this));
  this.v.layers['top'].addEventListener('mousedown', this.mousedown_handler.bind(this));
  this.v.layers['top'].addEventListener('mouseover', this.mouseover_handler.bind(this));
  this.v.layers['top'].addEventListener('mouseout',  this.mouseout_handler.bind(this));

  this.v.layers['top'].addEventListener('touchstart', this.touchstart_handler);
  this.v.layers['top'].addEventListener('touchend'  , this.touchend_handler);
  this.v.layers['top'].addEventListener('touchmove' , this.touchmove_handler);
}


///
/// File load routines
///
_via_ctrl.prototype.load_next_file = function() {
  var filecount = this.m.files.fileid_list.length;
  if ( filecount > 0) {
    var now_file_index = this.m.files.file_index_list[this.v.now.fileid];
    if ( now_file_index === (filecount-1) ) {
      this.load_file_from_index(0);
    } else {
      this.load_file_from_index(now_file_index + 1);
    }
  }
}

_via_ctrl.prototype.load_prev_file = function() {
  var filecount = this.m.files.fileid_list.length;
  if ( filecount > 0) {
    var now_file_index = this.m.files.file_index_list[this.v.now.fileid];
    if ( now_file_index === 0 ) {
      this.load_file_from_index( filecount - 1);
    } else {
      this.load_file_from_index(now_file_index - 1);
    }
  }
}

_via_ctrl.prototype.load_file_from_index = function( file_index ) {
  if ( file_index >= 0 || file_index < this.m.files.fileid_list.length ) {
    var fileid = this.m.files.fileid_list[file_index];
    this.load_file(fileid);
  }
}

_via_ctrl.prototype.load_file = function( fileid ) {
  if ( this.v.state_now === this.v.state.FILE_LOAD_ONGOING ) {
    this.show_message('Please wait until the current file load operation completes!');
    return;
  }

  if ( this.v.state_now !== this.v.state.IDLE ) {
    this.show_message('Cannot load new file when state is ' + this.v.state_now +
                      '. Press [Esc] to reset state');
    return;
  }

  if ( this.m.files.fileid_list.includes( fileid ) ) {
    this.v.set_state( this.v.state.FILE_LOAD_ONGOING );

    // 1. Load file contents (local image, url, base64)
    this.set_now_file(fileid).then( function() {
      this.compute_view_panel_to_nowfile_tform();
      this.update_file_info_for_nowfile();

      this.v.set_state( this.v.state.IDLE );
      this.update_layers_size_for_nowfile();

      // maintain a list of regions for current file
      this.v.now.all_rid_list = [];
      this.v.now.polygin_rid_list = [];
      this.v.now.other_rid_list = [];
      for ( var rid in this.m.regions[fileid] ) {
        if ( this.m.regions[fileid].hasOwnProperty(rid) ) {
          // maintain a list of regions for current file
          this.v.now.all_rid_list.push(rid);
          switch(this.m.regions[fileid][rid].shape) {
          case this.v.settings.REGION_SHAPE.POLYGON:
          case this.v.settings.REGION_SHAPE.POLYLINE:
            // region shape requiring more than two points (polygon, polyline)
            this.v.now.polygon_rid_list.push(rid);
            break;
          default:
            // region shapes requiring just two points (rectangle, circle, etc)
            this.v.now.other_rid_list.push(rid);
            break;
          }
        }
      }
      // load layer content, region shape and region attributes
      this.load_layer_content();
      this.load_layer_rshape();
      this.load_layer_rattr();
    }.bind(this), function(error) {
      this.v.set_state( this.v.state.IDLE );
      var filename = decodeURIComponent(this.m.files.metadata[fileid].filename);
      this.show_message('Error loading file : ' + filename);
      console.log(error);
    }.bind(this));
  }
}


///
/// Register handlers for events generated by the VIEW
///
_via_ctrl.prototype.set_region_shape = function(shape) {
  // deactivate the current shape button
  if ( this.v.now.region_shape ) {
    var old_html_element = document.getElementById( this.v.now.region_shape );
    if ( old_html_element ) {
      if ( old_html_element.classList.contains('active') ) {
        old_html_element.classList.remove('active');
      }
    }
  }

  this.v.now.region_shape = shape;
  var new_html_element = document.getElementById( this.v.now.region_shape );
  if ( new_html_element ) {
    if ( !new_html_element.classList.contains('active') ) {
      new_html_element.classList.add('active');
    }
  }

  // activate the new shape button
  //console.log('Setting region shape to : ' + this.m.now.region_shape);
}

///
/// file add/remove
///
_via_ctrl.prototype.add_user_sel_local_files = function(event) {
  event.stopPropagation();

  var user_selected_files = event.target.files;
  var all_promise = [];

  for ( var i = 0; i < user_selected_files.length; ++i ) {
    var promise = this.m.add_file_local( user_selected_files[i] );
    all_promise.push( promise );
  }

  var loaded_images_count    = 0;
  var loaded_videos_count    = 0;
  var discarded_files_count  = 0;

  Promise.all( all_promise ).then( function(result) {
    var first_fileid = '';
    for ( var i=0; i<result.length; i++ ) {
      if ( result[i].startsWith('Error' ) ) {
        discarded_files_count++;
      } else {
        var fileid = result[i];
        switch( this.m.files.metadata[fileid].type ) {
        case 'image':
	  loaded_images_count++;
	  break;
        case 'video':
	  loaded_videos_count++;
	  break;
        }

        if ( first_fileid === '' ) {
          first_fileid = fileid;
        }
      }
    }
    var msg = 'Loaded ' + loaded_images_count + ' images' +
	' and ' + loaded_videos_count + ' videos';
    if ( discarded_files_count !== 0 ) {
      msg += ' ( ' + discarded_files_count + ' discarded )';
    }
    this.show_message(msg);

    // the user is automatically taken to the first loaded file
    if ( first_fileid !== '' ) {
      this.load_file( first_fileid );
    }
  }.bind(this), function(error_msg) {
    console.log(error_msg);
  }.bind(this));

}

///
/// prompts for user input in web browser
///
_via_ctrl.prototype.select_local_files = function() {
  // ref: https://developer.mozilla.org/en-US/docs/Using_files_from_web_applications
  this.v.local_file_selector.click();
}

///
/// routines to update and maintain _via_view_panel layers
///
_via_ctrl.prototype.compute_view_panel_to_nowfile_tform = function() {
  // content's original width and height
  var cw, ch;
  switch( this.m.files.metadata[this.v.now.fileid].type ) {
  case 'image':
    cw = this.v.now.content.width;
    ch = this.v.now.content.height;
    break;
  case 'video':
    cw = this.v.now.content.videoWidth;
    ch = this.v.now.content.videoHeight;
    break;
  default:
    cw = 0;
    ch = 0;
    this.show_message('Cannot detemine the width and height of file content : ' +
                      this.m.files.metadata[this.m.now.fileid].filename);
  }

  if ( this.v.settings.theme.FILE_VIEW_SCALE_MODE === 'original' ) {
    this.v.now.tform.x = 0;
    this.v.now.tform.y = 0;
    this.v.now.tform.width  = cw;
    this.v.now.tform.height = ch;
    this.v.now.tform.content_width = cw;
    this.v.now.tform.content_height = ch;
    this.v.now.tform.scale = 1.0;
    this.v.now.tform.scale_inv = 1.0;
  } else {
    // content layer width and height
    var lw = this.v.view_panel.offsetWidth;
    var lh = this.v.view_panel.offsetHeight;

    // transformed content's position, width and height
    var txw = cw;
    var txh = ch;
    var txx = 0;
    var txy = 0;

    var scale_width = lw / cw;
    txw = lw;
    txh = ch * scale_width;

    if ( txh > lh ) {
      var scale_height = lh / txh;
      txh = lh;
      txw = txw * scale_height;
    }

    /*
    // scale the image only if the image size is greater than layer
    if ( cw > lw ) {
    // reduce content size so that it fits layer width
    var scale_width = lw / cw;
    txw = lw;
    txh = ch * scale_width;
    }

    if ( txh > lh ) {
    // content still does not fit the layer height
    // scale the height further
    var scale_height = lh / txh;
    txh = lh;
    txw = txw * scale_height;
    }
    */

    /*  */
    // determine the position of content on content_layer
    this.v.now.tform.x = Math.floor( (lw - txw)/2 ); // align to center
    //this.v.now.tform.x = 0;
    this.v.now.tform.y = Math.floor( (lh - txh)/2 );
    this.v.now.tform.width  = Math.floor(txw);
    this.v.now.tform.height = Math.floor(txh);
    this.v.now.tform.content_width = cw;
    this.v.now.tform.content_height = ch;
    this.v.now.tform.scale = this.v.now.tform.content_width / this.v.now.tform.width;
    this.v.now.tform.scale_inv = 1.0 / this.v.now.tform.scale;
  }
}

_via_ctrl.prototype.update_layers_size_for_nowfile = function() {
  var style = [];
  style.push('display: inline-block');
  style.push('position: absolute');
  style.push('overflow: hidden'); // needed for zoom-panel overflowing in boundary

  //style.push('top:  ' + this.v.now.tform.y + 'px'); // vertical alignment = center
  style.push('top: 0px'); // vertical alignment = top
  style.push('left: ' + this.v.now.tform.x + 'px');
  style.push('width:  ' + this.v.now.tform.width  + 'px');
  style.push('height: ' + this.v.now.tform.height + 'px');
  style.push('outline: none');

  var style_str = style.join(';');

  for ( var layer in this.v.layers ) {
    if ( this.v.layers.hasOwnProperty(layer) ) {
      this.v.layers[layer].setAttribute('style', style_str);
    }
  }
}

_via_ctrl.prototype.load_layer_content = function() {
  //this.v.layers['filecontent'].innerHTML = '';
  this.v.now.content.setAttribute('width', this.v.now.tform.width);
  this.v.now.content.setAttribute('height', this.v.now.tform.height);

  var child = this.v.layers['filecontent'].firstChild;
  if ( child ) {
    this.v.layers['filecontent'].replaceChild(this.v.now.content, child);
  } else {
    this.v.layers['filecontent'].appendChild(this.v.now.content);
  }
  /*
    if ( existing_content ) {
    this.v.layers['filecontent'].replaceChild(this.m.now.content, existing_content);
    } else {
    this.v.layers['filecontent'].appendChild(this.m.now.content);
    }
  */
}

_via_ctrl.prototype.load_layer_rshape = function() {
  // remove all existing regions
  this.remove_all_regions_from_view();

  // add regions from now
  var n = this.v.now.all_rid_list.length;
  for ( var i = 0; i < n; i++ ) {
    var rid = this.v.now.all_rid_list[i];
    this.add_region_to_view(rid);
  }
}

_via_ctrl.prototype.load_layer_rattr = function() {
  //@todo
}

///
/// routines to update and maintain general user interface elements
///
_via_ctrl.prototype.update_file_info_for_nowfile = function() {
  var now_fileindex = document.getElementById('now_fileindex');
  var now_filename  = document.getElementById('now_filename');

  if ( now_fileindex && now_filename ) {
    var file_index = this.m.files.file_index_list[this.v.now.fileid];
    var filename   = decodeURIComponent(this.m.files.metadata[this.v.now.fileid].filename);
    var file_count = this.m.files.fileid_list.length;
    //document.title = '[' + (file_index+1) + ' of ' + file_count + '] ' + this.m.files.metadata[fileid].filename;

    now_fileindex.innerHTML = (file_index + 1) + ' / ' + file_count;
    now_filename.innerHTML  = filename;
    now_filename.setAttribute('title', filename);
  }
}

_via_ctrl.prototype.show_message = function(msg, t) {
  if ( this.message_clear_timer ) {
    clearTimeout(this.message_clear_timer); // stop any previous timeouts
  }
  this.v.message_panel.innerHTML = msg;

  if ( t !== 0 ) {
    var timeout = t || this.v.settings.theme.MSG_TIMEOUT_MS;
    this.message_clear_timer = setTimeout( function() {
      this.v.message_panel.innerHTML = ' ';
    }.bind(this), timeout);
  }
}

///
/// keyboard input handlers
///
_via_ctrl.prototype.keyup_handler = function(e) {
  if (e.which === 16) {
    this.v.now.meta_keypress.shiftkey = false;
  }
  if ( e.which === 17 ) {
    this.v.now.meta_keypress.ctrlkey = false;
  }
  if (e.which === 18) {
    this.v.now.meta_keypress.altkey = false;
  }
}

_via_ctrl.prototype.keydown_handler = function(e) {
  e.stopPropagation();

  this.v.layers['top'].focus();
  e.preventDefault();

  if (e.which === 16) {
    this.v.now.meta_keypress.shiftkey = true;
  }
  if ( e.which === 17 ) {
    this.v.now.meta_keypress.ctrlkey = true;
  }
  if (e.which === 18) {
    this.v.now.meta_keypress.altkey = true;
  }

  // control commands
  if ( e.ctrlKey ) {
    this.v.now.meta_keypress.ctrlkey = true;
    if ( e.which === 83 ) { // Ctrl + s
      _via_ctrl_project_save_local();
      return;
    }
    if ( e.which === 79 ) { // Ctrl + o
      _via_ctrl_project_select_local_file();
      return;
    }
    if ( e.which === 65 ) { // Ctrl + a
      this.region_select_n( this.v.now.all_rid_list );
      this.v.set_state( this.v.state.REGION_SELECTED );
      return;
    }
    if ( e.which === 67 ) { // Ctrl + c
    }
    if ( e.which === 86 ) { // Ctrl + v
    }
  }

  if ( this.v.state_now == this.v.state.REGION_SELECTED ) {
    if( e.which === 46 || e.which === 8) { // Del or Backspace
      this.delete_selected_regions();
      return;
    }

    if ( e.which >= 37 && e.which <= 40 ) { // Arrow Keys
      // move all selected regions by 1 pixel
      var dx = 0;
      var dy = 0;
      // you can only move regions by 1 pixel (or, now.tform.scale) in the original image space
      if ( e.which === 39 ) { // right arrow
        dx = this.v.now.tform.scale_inv;
      }
      if ( e.which === 37 ) { // left arrow
        dx = -this.v.now.tform.scale_inv;
      }
      if ( e.which === 38 ) { // up arrow
        dy = -this.v.now.tform.scale_inv;
      }
      if ( e.which === 40 ) { // down arrow
        dy = this.v.now.tform.scale_inv;
      }

      if ( e.shiftKey ) {
        dx = dx * 10;
        dy = dy * 10;
      }

      if ( dx !== 0 || dy !== 0 ) {
        var n = this.v.now.region_select.rid_list.length;
        for ( var i=0; i<n; i++ ) {
          var fid = this.v.now.fileid;
          var rid = this.v.now.region_select.rid_list[i];

          this.m.region_move(fid, rid, dx, dy).then( function(result) {
	          this.remove_tmp_region_from_view();

	          if ( result.fileid === this.v.now.fileid ) {
              this.update_region_in_view( result.region_id );
	          }
          }.bind(this), function(error) {
            console.log(error);
          }.bind(this));
        }
        return;
      }
    }

    if ( e.which >= 27 ) { // Esc
      this.region_unselect_all();
      return;
    }
  }

  if ( this.v.state_now === this.v.state.IDLE ) {
    if ( e.which === 9 ) { // tab key
      if (e.shiftKey) {
        this.region_select_prev();
      } else {
        this.region_select_next();
      }
      return;
    }
  }

  if (e.which === 78 || e.which === 39 ) { // n or right arrow
    this.load_next_file();
    e.stopPropagation();
    return;
  }

  if (e.which === 80 || e.which === 37) { // p or left arrow
    this.load_prev_file();
    e.stopPropagation();
    return;
  }

  if (e.which === 121) { // F10 for debug messages
    e.stopPropagation();
    console.log('State = ' + this.v.state_now);
    console.log(this.m.regions[this.v.now.fileid]);
    console.log(this.m.files);
    console.log(this.v.now);
    return;
  }

  if (e.which === 13 ) { // Enter key
    e.stopPropagation();
    if ( this.v.state_now === this.v.state.REGION_DRAW_NCLICK_ONGOING ) {
      this.add_region_from_nvertex();
      this.v.nvertex.splice(0);
      this.v.set_state( this.v.state.IDLE );
    }
    return;
  }

  if (e.which === 27 ) { // Esc key
    e.stopPropagation();
    this.v.nvertex = [];
    this.remove_tmp_region_from_view();
    this.v.set_state( this.v.state.IDLE );
    return;
  }
}

///
/// Mouse and touch event hanlders
///
_via_ctrl.prototype.mousemove_handler = function(e) {
  e.stopPropagation();

  var x1 = e.offsetX;
  var y1 = e.offsetY;
  this.v.last.mousemove.x = x1;
  this.v.last.mousemove.y = y1;

  var nvertex_tmp = this.v.nvertex.slice(0);
  nvertex_tmp.push( x1 );
  nvertex_tmp.push( y1 );

  if(this.v.zoom.is_enabled) {
    this.update_zoom_panel_contents();
  }

  if ( this.v.state_now === this.v.state.REGION_DRAW_ONGOING ) {
    // region shape requiring than two points (rectangle, circle, ellipse, etc)
    this.add_tmp_region_to_view(nvertex_tmp);
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_DRAW_NCLICK_ONGOING ) {
    // region shape that may require more than two points (polyline, polygon)
    this.add_tmp_region_to_view(nvertex_tmp);
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_DRAW_OR_UNSELECT_POSSIBLE ) {
    var p0 = this.v.last.mousedown;
    var p1 = new _via_point(x1, y1);
    if ( !this.are_mouse_events_nearby( p0, p1 ) ) {
      this.region_unselect_all();
      this.v.set_state( this.v.state.REGION_DRAW_ONGOING );
    }
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_MOVE_ONGOING ) {
    // region shape that may require more than two points (polyline, polygon)
    var dx = nvertex_tmp[2] - nvertex_tmp[0];
    var dy = nvertex_tmp[3] - nvertex_tmp[1];
    this.tmp_move_selected_region_in_view(dx, dy);
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_RESIZE_ONGOING ) {
    // region shape that may require more than two points (polyline, polygon)
    var edge_id = this.v.last.clicked_region_edge_id;
    this.tmp_resize_selected_region_in_view(edge_id, x1, y1);
    return;
  }

  // mousedown inside an existing region and mouse being moved indicates region draw operation
  if ( this.v.state_now === this.v.state.REGION_SELECT_OR_DRAW_POSSIBLE ) {
    var p0 = this.v.last.mousedown;
    var p1 = new _via_point(x1, y1);
    if ( !this.are_mouse_events_nearby( p0, p1 ) ) {
      this.v.nvertex.push( this.v.last.mousedown.x );
      this.v.nvertex.push( this.v.last.mousedown.y );
      this.v.set_state( this.v.state.REGION_DRAW_ONGOING );
    }
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_SELECTED ) {
    var fileid = this.v.now.fileid;
    var n = this.v.now.region_select.rid_list.length;
    for ( var i=0; i<n; i++ ) {
      var rid = this.v.now.region_select.rid_list[i];
      var tolerance = this.v.settings.theme.MOUSE_VERTEX_TOLERANCE;
      var edge_id = this.m.is_on_region_edge(fileid, rid, x1, y1, tolerance );
      if ( edge_id === -1 ) {
        // not on edge, is mouse inside the selected region?
        var is_inside_selected_region = this.m.is_point_inside_region(fileid, rid, x1, y1);
        if ( is_inside_selected_region ) {
          this.v.layers.top.style.cursor = 'move';
          return;
        }
      } else {
        // mouse cursor is on the edge
        var r = this.m.regions[this.v.now.fileid][rid];
        switch( r.shape ) {
        case this.v.settings.REGION_SHAPE.RECT:
        case this.v.settings.REGION_SHAPE.CIRCLE:
        case this.v.settings.REGION_SHAPE.ELLIPSE:
          switch( edge_id ) {
          case 1: // corner top-left
          case 5: // corner bottom-right
            this.v.layers.top.style.cursor = 'nwse-resize';
            break;
          case 3:
          case 7:
            this.v.layers.top.style.cursor = 'nesw-resize';
            break;
          case 2:
          case 6:
            this.v.layers.top.style.cursor = 'row-resize';
            break;
          case 4:
          case 8:
            this.v.layers.top.style.cursor = 'col-resize';
            break;
          }
          break;
        case this.v.settings.REGION_SHAPE.LINE:
        case this.v.settings.REGION_SHAPE.POLYLINE:
        case this.v.settings.REGION_SHAPE.POLYGON:
          //this.v.layers.top.style.cursor = 'move';
          this.v.layers.top.style.cursor = 'cell';
          break;
        }
        return;
      }
    }
    this.v.layers.top.style.cursor = 'default';
    return;
  }

  if ( this.v.state_now === this.v.state.SELECT_ALL_INSIDE_AN_AREA_ONGOING ) {
    this.add_tmp_region_to_view(nvertex_tmp,
                                this.v.settings.REGION_SHAPE.RECT,
                                this.v.settings.theme.svg.ON_SELECT_AREA_DRAW);
    return;
  }
}

_via_ctrl.prototype.mouseup_handler = function(e) {
  e.stopPropagation();
  var x1 = e.offsetX;
  var y1 = e.offsetY;
  this.v.last.mouseup = new _via_point(x1, y1);
  this.v.layers.top.focus();

  if ( this.v.state_now === this.v.state.REGION_DRAW_ONGOING ) {
    switch ( this.v.now.region_shape ) {
    case this.v.settings.REGION_SHAPE.POLYGON:
    case this.v.settings.REGION_SHAPE.POLYLINE:
      // region shape requiring more than two points (polygon, polyline)
      this.v.set_state( this.v.state.REGION_DRAW_NCLICK_ONGOING );
      break;

    default:
      // region shape requiring just two points (rectangle, circle, ellipse, etc)
      // check if the two vertices are malformed
      var p0 = this.v.last.mousedown;
      var p1 = this.v.last.mouseup;
      var is_click = this.are_mouse_events_nearby( p0, p1 );

      if ( !is_click || this.v.now.region_shape === this.v.settings.REGION_SHAPE.POINT ) {
        this.v.nvertex.push( x1 );
        this.v.nvertex.push( y1 );
        this.add_region_from_nvertex();
        this.remove_tmp_region_from_view();
      }
      this.v.nvertex.splice(0);
      this.v.set_state( this.v.state.IDLE );
    }
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_SELECT_OR_DRAW_POSSIBLE ) {
    // region select
    var rid = this.v.last.clicked_region_id;
    this.region_unselect_all();
    this.region_select(rid);
    this.v.now.region_select.fileid = this.v.now.fileid;
    this.v.set_state( this.v.state.REGION_SELECTED );

    if(this.v.zoom.is_enabled) {
      this.update_zoom_panel_contents();
    }

    this.show_message( 'Click and drag inside region to <span class="highlight">move</span>, ' +
                       'at region edge to <span class="highlight">resize</span>, ' +
		       'Move region by small amount using <span class="highlight">keyboard arrow keys</span>.');
  }

  if ( this.v.state_now === this.v.state.REGION_UNSELECT_ONGOING ) {
    this.region_unselect_all();
    this.v.set_state( this.v.state.IDLE );
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_MOVE_ONGOING ) {
    this.v.nvertex.push( x1 );
    this.v.nvertex.push( y1 );
    var nvertex = this.v.nvertex.slice(0);

    var dx = nvertex[2] - nvertex[0];
    var dy = nvertex[3] - nvertex[1];

    var n = this.v.now.region_select.rid_list.length;
    for ( var i=0; i<n; i++ ) {
      var srid = this.v.now.region_select.rid_list[i];
      this.m.region_move(this.v.now.fileid, srid, dx, dy).then( function(result) {
        var srid_tmp = this.v.now.view_tmp_region.RID_PREFIX_MOVE + result.region_id;
        this.remove_tmp_region_from_view(srid_tmp);

        if ( result.fileid === this.v.now.fileid ) {
          this.update_region_in_view( result.region_id );
        }
        this.trigger_hook(this.hook.id.REGION_MOVED, {'fileid': result.fileid, 'rid': result.region_id});
      }.bind(this), function(error) {
        console.log(error);
      }.bind(this));
    }
    this.v.nvertex.splice(0);
    this.v.set_state( this.v.state.REGION_SELECTED );
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_RESIZE_ONGOING ) {
    var edge_id = this.v.last.clicked_region_edge_id;
    var edge_rid = this.v.last.clicked_region_id;
    this.m.region_resize(this.v.now.fileid, edge_rid, edge_id, x1, y1).then( function(result) {
      var rid_tmp = this.v.now.view_tmp_region.RID_PREFIX_RESIZE + edge_rid;
      this.remove_tmp_region_from_view(rid_tmp);

      if ( result.fileid === this.v.now.fileid ) {
        this.update_region_in_view( result.region_id );
      }
      this.trigger_hook(this.hook.id.REGION_RESIZED, {'fileid': result.fileid, 'rid': result.region_id});
    }.bind(this), function(error) {
      console.log(error);
    }.bind(this));
    this.v.set_state( this.v.state.REGION_SELECTED );
    return;
  }

  if ( this.v.state_now === this.v.state.SELECT_ALL_INSIDE_AN_AREA_ONGOING ) {
    var x0 = this.v.last.mousedown.x;
    var y0 = this.v.last.mousedown.y;
    var rid_list = this.get_regions_inside_an_area(x0, y0, x1, y1);

    if ( rid_list.length ) {
      this.v.now.region_select.fileid = this.v.now.fileid;
      this.region_select_n( rid_list );
      this.v.set_state( this.v.state.REGION_SELECTED );
    } else {
      this.v.set_state( this.v.state.IDLE );
    }
    this.v.nvertex.splice(0);
    this.remove_tmp_region_from_view();
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_SELECT_TOGGLE_ONGOING ) {
    var rid = this.v.last.clicked_region_id;
    this.region_select_toggle(rid);
    if ( this.v.now.region_select.rid_list.length === 0 ) {
      // region toggle resulted is no selected region
      this.v.set_state( this.v.state.IDLE );
      this.v.layers.top.style.cursor = 'default';
    } else {
      this.v.set_state( this.v.state.REGION_SELECTED );
    }
    return;
  }
}

_via_ctrl.prototype.mousedown_handler = function(e) {
  e.stopPropagation();

  var x0 = e.offsetX;
  var y0 = e.offsetY;
  this.v.last.mousedown = new _via_point(x0, y0);
  if ( this.v.state_now === this.v.state.IDLE ) {
    if ( this.v.now.meta_keypress.shiftkey ) {
      this.v.nvertex.push( x0 );
      this.v.nvertex.push( y0 );
      this.v.set_state( this.v.state.SELECT_ALL_INSIDE_AN_AREA_ONGOING );
    } else {
      // is this mousedown inside a region?
      var fileid = this.v.now.fileid;
      this.v.last.clicked_region_id = this.m.is_point_in_a_region(fileid, x0, y0);
      if ( this.v.last.clicked_region_id !== '' ) {
        // two possibilities:
        // 1. Draw region inside an existing region
        // 2. Select the region
        this.v.set_state( this.v.state.REGION_SELECT_OR_DRAW_POSSIBLE );
      } else {
        this.v.nvertex.push( x0 );
        this.v.nvertex.push( y0 );
        this.v.set_state( this.v.state.REGION_DRAW_ONGOING );
      }
    }
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_DRAW_NCLICK_ONGOING ) {
    this.v.nvertex.push( x0 );
    this.v.nvertex.push( y0 );
    this.show_message( 'Press <span class="highlight">[Enter]</span> key when you finish drawing, ' +
		       '<span class="highlight">[Esc]</span> to cancel.');
    return;
  }

  if ( this.v.state_now === this.v.state.REGION_SELECTED ) {
    var fileid = this.v.now.fileid;
    var tolerance = this.v.settings.theme.MOUSE_VERTEX_TOLERANCE;
    var selected_rid_list = this.v.now.region_select.rid_list;

    var edge = this.m.is_on_these_region_edge(fileid, selected_rid_list, x0, y0, tolerance);
    if ( edge.id !== -1 ) {
      // mousedown on region edge
      this.v.last.clicked_region_id = edge.rid;
      this.v.last.clicked_region_edge_id = edge.id;
      this.v.set_state( this.v.state.REGION_RESIZE_ONGOING );
    } else {
      if ( this.v.now.meta_keypress.shiftkey ) {
        this.v.last.clicked_region_id = this.m.is_point_inside_these_regions(fileid, this.v.now.all_rid_list, x0, y0);
        if ( this.v.last.clicked_region_id !== '' ) {
          // inside a region, hence toggle region selection
          this.v.set_state( this.v.state.REGION_SELECT_TOGGLE_ONGOING );
        } else {
          // user intends to draw an area to select all regions inside the area
          this.v.set_state( this.v.state.SELECT_ALL_INSIDE_AN_AREA_ONGOING );
        }
      } else {
        this.v.last.clicked_region_id = this.m.is_point_inside_these_regions(fileid, selected_rid_list, x0, y0);
        if ( this.v.last.clicked_region_id !== '' ) {
          // mousedown inside one of the selected regions
          this.v.nvertex.push( x0 );
          this.v.nvertex.push( y0 );
          this.v.set_state( this.v.state.REGION_MOVE_ONGOING );
        } else {
          this.v.set_state( this.v.state.REGION_UNSELECT_ONGOING );
        }
      }
    }
    return;
  }
}

_via_ctrl.prototype.mouseover_handler = function(e) {
  e.stopPropagation();
  if(this.v.zoom.is_enabled) {
    this.zoom_add_panel();
  }
}

_via_ctrl.prototype.mouseout_handler = function(e) {
  e.stopPropagation();
  if(this.v.zoom.is_enabled) {
    this.zoom_remove_panel();
  }
}

_via_ctrl.prototype.touchstart_handler = function(e) {
  e.stopPropagation();
}

_via_ctrl.prototype.touchend_handler = function(e) {
  e.stopPropagation();
}

_via_ctrl.prototype.touchmove_handler = function(e) {
  e.stopPropagation();
}

///
/// Region Draw Operations
///
_via_ctrl.prototype.nvertex_clear = function() {
  this.v.nvertex.splice(0);
}

_via_ctrl.prototype.add_region_from_nvertex = function() {
  var nvertex = this.v.nvertex.slice(0);
  var shape   = this.v.now.region_shape;
  var fileid  = this.v.now.fileid;
  var tform_scale = this.v.now.tform.scale;
  this.m.region_add(fileid, tform_scale, shape, nvertex).then( function(rid) {
    // maintain a list of regions for current file
    this.v.now.all_rid_list.push(rid);
    switch(shape) {
    case this.v.settings.REGION_SHAPE.POLYGON:
    case this.v.settings.REGION_SHAPE.POLYLINE:
      // region shape requiring more than two points (polygon, polyline)
      this.v.now.polygon_rid_list.push(rid);
      break;
    default:
      // region shapes requiring just two points (rectangle, circle, etc)
      this.v.now.other_rid_list.push(rid);
      break;
    }

    this.v.last.added_region_id = rid;
    this.add_region_to_view(rid);

    this.trigger_hook(this.hook.id.REGION_ADDED, {'fileid': fileid, 'rid': rid});
  }.bind(this), function(error) {
    console.log(error);
  }.bind(this) );
}

_via_ctrl.prototype.remove_all_regions_from_view = function() {
  this.v.layers['rshape'].innerHTML = '';
}

_via_ctrl.prototype.add_region_to_view = function(rid) {
  var region_svg = this.m.regions[this.v.now.fileid][rid].get_svg_element();
  this.svg_apply_theme(region_svg, this.v.settings.theme.svg.REGION);
  this.v.layers['rshape'].appendChild(region_svg);

  if(this.v.zoom.is_enabled) {
    this.update_zoom_panel_contents();
  }
}

_via_ctrl.prototype.del_region_from_view = function(rid) {
  var r = this.v.layers.rshape.getElementById( rid );
  if ( r ) {
    this.v.layers.rshape.removeChild( r );
  }

  if(this.v.zoom.is_enabled) {
    this.update_zoom_panel_contents();
  }
}

_via_ctrl.prototype.update_region_in_view = function(rid) {
  var region_svg = this.v.layers.rshape.getElementById(rid);
  if ( region_svg ) {
    var new_region = this.m.regions[this.v.now.fileid][rid];
    var n = new_region.svg_attributes.length;
    for ( var i = 0; i < n; i++ ) {
      var attr = new_region.svg_attributes[i];
      region_svg.setAttributeNS(null, attr, new_region[attr]);
    }

    if(this.v.zoom.is_enabled) {
      this.update_zoom_panel_contents();
    }
  }
}

_via_ctrl.prototype.svg_apply_theme = function(svg_element, theme) {
  for ( var p in theme ) {
    if ( theme.hasOwnProperty(p) ) {
      svg_element.setAttributeNS(null, p, theme[p]);
    }
  }
}

_via_ctrl.prototype.add_tmp_region_to_view = function(nvertex_tmp,
                                                      shape,
                                                      svg_style,
                                                      rid_tmp) {
  var rid_tmp   = rid_tmp || this.v.now.view_tmp_region.DEFAULT_RID;
  var shape     = shape || this.v.now.region_shape;
  var svg_style = svg_style || this.v.settings.theme.svg.ON_DRAWING;
  var old_region_tmp = this.v.layers.rshape.getElementById( rid_tmp );
  if ( old_region_tmp ) {
    // updating existing region is more efficient than replacing it
    this.update_tmp_svg_region(old_region_tmp, nvertex_tmp);
  } else {
    var region_tmp = this.create_tmp_svg_region(shape, nvertex_tmp);
    this.svg_apply_theme(region_tmp, svg_style);

    this.v.layers.rshape.appendChild(region_tmp);
  }
}

_via_ctrl.prototype.tmp_move_selected_region_in_view = function(dx, dy) {
  var n = this.v.now.region_select.rid_list.length;
  for ( var i=0; i<n; i++ ) {
    var srid = this.v.now.region_select.rid_list[i];
    var sregion = this.m.regions[this.v.now.fileid][srid];
    var srid_tmp = this.v.now.view_tmp_region.RID_PREFIX_MOVE + srid;

    var svg_element = this.v.layers.rshape.getElementById( srid_tmp );
    if ( svg_element ) {
      // update the svg element
      this.move_tmp_svg_region( svg_element, sregion, dx, dy );
    } else {
      // create temporary region
      var new_svg_element = this.create_tmp_svg_region(sregion.shape, sregion.dview, srid_tmp);
      this.move_tmp_svg_region(new_svg_element, sregion, dx, dy);
      this.svg_apply_theme(new_svg_element, this.v.settings.theme.svg.ON_MOVE);
      this.v.layers.rshape.appendChild(new_svg_element);
    }
  }
}

_via_ctrl.prototype.tmp_resize_selected_region_in_view = function(edge_id, new_x, new_y) {
  var srid = this.v.last.clicked_region_id;
  var sregion = this.m.regions[this.v.now.fileid][srid];
  var srid_tmp = this.v.now.view_tmp_region.RID_PREFIX_RESIZE + srid;

  var svg_element = this.v.layers.rshape.getElementById( srid_tmp );
  if ( svg_element ) {
    // update the svg element
    this.resize_tmp_svg_region( svg_element, sregion, edge_id, new_x, new_y );
  } else {
    // create temporary region
    var new_svg_element = this.create_tmp_svg_region(sregion.shape, sregion.dview, srid_tmp);
    this.resize_tmp_svg_region(new_svg_element, sregion, edge_id, new_x, new_y);
    this.svg_apply_theme(new_svg_element, this.v.settings.theme.svg.ON_MOVE);
    this.v.layers.rshape.appendChild(new_svg_element);
  }
}

_via_ctrl.prototype.remove_tmp_region_from_view = function(rid) {
  var rid_tmp = rid || this.v.now.view_tmp_region.DEFAULT_RID;
  var region_tmp = this.v.layers.rshape.getElementById( rid_tmp );
  if ( region_tmp ) {
    this.v.layers.rshape.removeChild( region_tmp );
  }
}

///
/// Region management
///
_via_ctrl.prototype.delete_selected_regions = function() {
  this.region_delete( this.v.now.region_select.rid_list );
  this.v.now.region_select.rid_list = [];
  this.v.now.region_select.fileid = '';
  this.v.set_state( this.v.state.IDLE );
}

_via_ctrl.prototype.region_delete = function(rid_list) {
  var n = rid_list.length;
  var fileid = this.v.now.fileid;
  for ( var i=0; i<n; i++ ) {
    var rid = rid_list[i];
    var shape = this.m.regions[fileid][rid].shape;
    delete this.m.regions[fileid][rid];

    this.v.now.all_rid_list.splice( this.v.now.all_rid_list.indexOf(rid), 1 );
    if ( shape === this.v.settings.REGION_SHAPE.POLYGON ) {
      this.v.now.polygon_rid_list.splice( this.v.now.polygon_rid_list.indexOf(rid), 1 );
    } else {
      this.v.now.other_rid_list.splice( this.v.now.other_rid_list.indexOf(rid), 1 );
    }

    // delete region from view
    this.del_region_from_view(rid);
  }

  if(this.v.zoom.is_enabled) {
    this.update_zoom_panel_contents();
  }
}


///
/// maintainers for 'now' : the current file being annotated
///
_via_ctrl.prototype.set_now_file = function( fileid ) {
  return new Promise( function(ok_callback, err_callback) {
    var filereader = new FileReader();
    filereader.addEventListener( 'load', function() {
      this.v.now.fileid = fileid;

      switch( this.m.files.metadata[fileid].type ) {
      case 'image':
        this.v.now.content = document.createElement('img');
        this.v.now.content.setAttribute('id', fileid);
        this.v.now.content.addEventListener('load', function() {
	  ok_callback();
        }, false);

        break;
      case 'video':
        this.v.now.content = document.createElement('video');
        this.v.now.content.setAttribute('id', fileid);
        this.v.now.content.setAttribute('autoplay', 'true');
        this.v.now.content.setAttribute('loop', 'true');
        this.v.now.content.addEventListener('canplay', function() {
	  ok_callback();
        }, false);
        break;

      default:
        err_callback('Unknown content type [' + this.m.files.metadata[fileid].type +
                     '] for file : ' + this.m.files.metadata[this.v.now.fileid].filename);
      }
      this.v.now.content.addEventListener( 'error', err_callback);
      this.v.now.content.addEventListener( 'abort', err_callback);

      this.v.now.content.src = filereader.result;
    }.bind(this));

    filereader.addEventListener( 'error', err_callback);
    filereader.addEventListener( 'abort', err_callback);

    if ( this.m.files.metadata[fileid].source === 'local' ) {
      filereader.readAsDataURL( this.m.files.content[fileid] );
    } else {
      filereader.readAsText( new Blob([this.m.files.content[fileid]]) );
    }
  }.bind(this));
}

///
/// utility functions
///
_via_ctrl.prototype.file_hash = function(filename, filesize, frame, count) {
  var fileid_str = filename + (filesize || -1 ) + (frame || 0) + (count || 1);

  // @@todo: fixme
  // avoid crypto.subtle.digest() because it is not allowed over http:// connection by chrome
  //return this.hash( fileid_str );

  return new Promise( function(ok_callback, err_callback) {
    ok_callback(fileid_str);
  });
}

_via_ctrl.prototype.url_filetype = function(url) {
  var dot_index = url.lastIndexOf('.');
  var ext = url.substr(dot_index);

  var type = '';
  switch( ext.toLowerCase() ) {
  case '.webm':
  case '.ogg':
  case '.mp4':
  case '.ogv':
    type = 'video';
    break;
  case '.jpg':
  case '.png':
  case '.jpeg':
  case '.svg':
    type = 'image'
    break;

  default:
    type = 'unknown';
  }
  return type;
}

_via_ctrl.prototype.are_mouse_events_nearby = function(p0, p1) {
  var dx = p1.x - p0.x;
  var dy = p1.y - p0.y;
  if ( ( dx*dx + dy*dy ) <= this.v.settings.theme.MOUSE_CLICK_TOLERANCE2 ) {
    return true;
  } else {
    return false;
  }
}

// to draw svg shape as the region is being drawn, moved or resized by the user
_via_ctrl.prototype.create_tmp_svg_region = function(shape, nvertex, rid_tmp) {
  var _VIA_SVG_NS = 'http://www.w3.org/2000/svg';
  var svg_element = document.createElementNS(_VIA_SVG_NS, shape);
  var rid_tmp = rid_tmp || this.v.now.view_tmp_region.DEFAULT_RID;

  svg_element.setAttributeNS(null, 'id', rid_tmp);

  var dx = Math.abs(nvertex[2] - nvertex[0]);
  var dy = Math.abs(nvertex[3] - nvertex[1]);

  switch(shape) {
  case this.v.settings.REGION_SHAPE.RECT:
    // ensure that (x0,y0) corresponds to top-left corner of rectangle
    var x0 = nvertex[0];
    var x1 = nvertex[2];
    var y0 = nvertex[1];
    var y1 = nvertex[3];
    if ( nvertex[0] > nvertex[2] ) {
      x0 = nvertex[2];
      x1 = nvertex[0];
    }
    if ( nvertex[1] > nvertex[3] ) {
      y0 = nvertex[3];
      y1 = nvertex[1];
    }

    svg_element.setAttributeNS(null, 'x', x0);
    svg_element.setAttributeNS(null, 'y', y0);
    svg_element.setAttributeNS(null, 'width',  x1 - x0);
    svg_element.setAttributeNS(null, 'height', y1 - y0);
    break;
  case this.v.settings.REGION_SHAPE.CIRCLE:
    var r  = Math.round( Math.sqrt( dx * dx + dy * dy ) );
    svg_element.setAttributeNS(null, 'cx', nvertex[0]);
    svg_element.setAttributeNS(null, 'cy', nvertex[1]);
    svg_element.setAttributeNS(null, 'r', r);
    break;
  case this.v.settings.REGION_SHAPE.ELLIPSE:
    svg_element.setAttributeNS(null, 'cx', nvertex[0]);
    svg_element.setAttributeNS(null, 'cy', nvertex[1]);
    svg_element.setAttributeNS(null, 'rx', dx);
    svg_element.setAttributeNS(null, 'ry', dy);
    break;
  case this.v.settings.REGION_SHAPE.LINE:
    // preserve the start(x1,y1) and end(x2,y2) points as drawn by user
    svg_element.setAttributeNS(null, 'x1', nvertex[0]);
    svg_element.setAttributeNS(null, 'y1', nvertex[1]);
    svg_element.setAttributeNS(null, 'x2', nvertex[2]);
    svg_element.setAttributeNS(null, 'y2', nvertex[3]);
    break;
  case this.v.settings.REGION_SHAPE.POLYLINE:
  case this.v.settings.REGION_SHAPE.POLYGON:
    var n = nvertex.length;
    var points = [];
    for ( var i = 0; i < n; i += 2 ) {
      points.push(nvertex[i] + ' ' + nvertex[i+1]);
    }
    svg_element.setAttributeNS(null, 'points', points.join(','));
    break;
  case this.v.settings.REGION_SHAPE.POINT:
    svg_element.setAttributeNS(null, 'cx', nvertex[0]);
    svg_element.setAttributeNS(null, 'cy', nvertex[1]);
    svg_element.setAttributeNS(null, 'r', this.v.settings.theme.svg.POINT_SHAPE_DRAW_RADIUS);
    break;
  }
  return svg_element;
}

_via_ctrl.prototype.update_tmp_svg_region = function(svg_element, nvertex) {
  var dx = Math.abs(nvertex[2] - nvertex[0]);
  var dy = Math.abs(nvertex[3] - nvertex[1]);

  switch( svg_element.localName ) {
  case this.v.settings.REGION_SHAPE.RECT:
    // ensure that (x0,y0) corresponds to top-left corner of rectangle
    var x0 = nvertex[0];
    var x1 = nvertex[2];
    var y0 = nvertex[1];
    var y1 = nvertex[3];
    if ( nvertex[0] > nvertex[2] ) {
      x0 = nvertex[2];
      x1 = nvertex[0];
    }
    if ( nvertex[1] > nvertex[3] ) {
      y0 = nvertex[3];
      y1 = nvertex[1];
    }

    svg_element.setAttributeNS(null, 'x', x0);
    svg_element.setAttributeNS(null, 'y', y0);
    svg_element.setAttributeNS(null, 'width',  x1 - x0);
    svg_element.setAttributeNS(null, 'height', y1 - y0);
    break;
  case this.v.settings.REGION_SHAPE.CIRCLE:
    var r  = Math.round( Math.sqrt( dx * dx + dy * dy ) );
    svg_element.setAttributeNS(null, 'r', r);
    break;
  case this.v.settings.REGION_SHAPE.ELLIPSE:
    svg_element.setAttributeNS(null, 'rx', dx);
    svg_element.setAttributeNS(null, 'ry', dy);
    break;
  case this.v.settings.REGION_SHAPE.LINE:
    // preserve the start(x1,y1) and end(x2,y2) points as drawn by user
    svg_element.setAttributeNS(null, 'x2', nvertex[2]);
    svg_element.setAttributeNS(null, 'y2', nvertex[3]);
    break;
  case this.v.settings.REGION_SHAPE.POLYLINE:
  case this.v.settings.REGION_SHAPE.POLYGON:
    var points = svg_element.getAttributeNS(null, 'points');
    var n = nvertex.length;
    var points = [];
    for ( var i = 0; i < n; i += 2 ) {
      points.push( nvertex[i] + ' ' + nvertex[i+1] );
    }
    svg_element.setAttributeNS(null, 'points', points.join(','));
    break;
  case this.v.settings.REGION_SHAPE.POINT:
    svg_element.setAttributeNS(null, 'cx', nvertex[0]);
    svg_element.setAttributeNS(null, 'cy', nvertex[1]);
    svg_element.setAttributeNS(null, 'r', this.v.settings.theme.svg.POINT_SHAPE_DRAW_RADIUS);
    break;
  }
}

_via_ctrl.prototype.move_tmp_svg_region = function(svg_element, region, dx, dy) {

  switch( svg_element.localName ) {
  case this.v.settings.REGION_SHAPE.RECT:
    svg_element.setAttributeNS(null, 'x', region.x + dx);
    svg_element.setAttributeNS(null, 'y', region.y + dy);
    break;
  case this.v.settings.REGION_SHAPE.CIRCLE:
  case this.v.settings.REGION_SHAPE.ELLIPSE:
  case this.v.settings.REGION_SHAPE.POINT:
    svg_element.setAttributeNS(null, 'cx', region.cx + dx);
    svg_element.setAttributeNS(null, 'cy', region.cy + dy);
    break;
  case this.v.settings.REGION_SHAPE.LINE:
    svg_element.setAttributeNS(null, 'x1', region.x1 + dx);
    svg_element.setAttributeNS(null, 'y1', region.y1 + dy);
    svg_element.setAttributeNS(null, 'x2', region.x2 + dx);
    svg_element.setAttributeNS(null, 'y2', region.y2 + dy);
    break;
  case this.v.settings.REGION_SHAPE.POLYLINE:
  case this.v.settings.REGION_SHAPE.POLYGON:
    var points = [];
    var n = region.dview.length;
    for ( var i = 0; i < n; i += 2 ) {
      points.push( (region.dview[i] + dx) + ' ' + (region.dview[i+1] + dy) );
    }
    svg_element.setAttributeNS(null, 'points', points.join(','));
    break;
  }
}

_via_ctrl.prototype.resize_tmp_svg_region = function(svg_element, region, edge_id, new_x, new_y, rid_tmp) {

  // WARNING: do not invoke resize in the original region!
  // create a copy of region
  var rid_tmp = rid_tmp || this.v.now.view_tmp_region.DEFAULT_RID;
  var r = new _via_region(region.shape,
                          rid_tmp,
                          region.dview,
                          1);
  r.resize(edge_id, new_x, new_y);

  var new_svg_element = r.get_svg_element();
  var n = r.svg_attributes.length;
  for (var i=0; i<n; i++ ) {
    var p = r.svg_attributes[i];
    var old_value = svg_element.getAttributeNS(null, p);
    var new_value = new_svg_element.getAttributeNS(null, p);
    if ( new_value !== old_value ) {
      svg_element.setAttributeNS(null, p, new_value);
    }
  }
}

///
/// Zoom support
///
_via_ctrl.prototype.zoom_remove_panel = function() {
  this.v.layers['zoom'].innerHTML = '';
}
_via_ctrl.prototype.zoom_add_panel = function() {
  this.v.zoom.container = document.createElement('div');
  this.v.zoom.container.setAttribute('id', this.v.zoom.container_id);

  // add filecontent
  this.v.zoom.filecontent = this.v.layers['filecontent'].cloneNode(true);
  this.v.zoom.filecontent.setAttribute('id', 'zoom_filecontent');
  this.v.zoom.filecontent.removeAttribute('style');
  this.v.zoom.filecontent.childNodes[0].removeAttribute('id');
  this.v.zoom.filecontent.childNodes[0].removeAttribute('style');

  var fcw = this.v.now.tform.width * this.v.zoom.scale;
  var fch = this.v.now.tform.height * this.v.zoom.scale;
  this.v.zoom.filecontent.childNodes[0].setAttribute('width', fcw);
  this.v.zoom.filecontent.childNodes[0].setAttribute('height', fch);

  this.v.layers['zoom'].appendChild(this.v.zoom.container);
}
_via_ctrl.prototype.zoom_activate = function() {
  if ( this.v.zoom.is_enabled ) {
    // change zoom scale if already activated
    this.v.zoom.scale += 0.5;
  } else {
    this.v.zoom.is_enabled = true;
  }
  this.show_message('Zoom <span class="highlight">' + this.v.zoom.scale + 'X</span> enabled');
  // handled by mouseover_handler()
  //this.zoom_add_panel();
}

_via_ctrl.prototype.zoom_deactivate = function() {
  this.v.zoom.is_enabled = false;
  this.v.zoom.scale = this.v.zoom.DEFAULT_SCALE;
  this.show_message('Zoom disabled');
  // handled by mouseout_handler()
  //this.zoom_remove_panel();
}

_via_ctrl.prototype.update_zoom_panel_contents = function() {
  var px = this.v.last.mousemove.x;
  var py = this.v.last.mousemove.y;

  // position zoom panel
  var zoom_panel_left = px - this.v.zoom.sizeby2;
  var zoom_panel_top  = py - this.v.zoom.sizeby2;
  var style = [];
  style.push('position: absolute');
  style.push('overflow: hidden'); // without this, zoom will break
  style.push('width:' + this.v.zoom.size + 'px');
  style.push('height:' + this.v.zoom.size + 'px');
  style.push('top:' + zoom_panel_top + 'px');
  style.push('left:' + zoom_panel_left + 'px');
  style.push('border: 1px solid red');
  style.push('border-radius:' + this.v.zoom.sizeby2 + 'px');
  this.v.zoom.container.setAttribute('style', style.join(';'));

  // position filecontent
  style = [];
  style.push('position: absolute');
  var scaled_img_left = this.v.zoom.sizeby2 - px * this.v.zoom.scale;
  var scaled_img_top  = this.v.zoom.sizeby2 - py * this.v.zoom.scale;
  style.push('top:' + scaled_img_top + 'px');
  style.push('left:' + scaled_img_left + 'px');
  this.v.zoom.filecontent.childNodes[0].setAttribute('style', style.join(';'));

  // position rshape
  var rshape = this.v.layers['rshape'].cloneNode(true);
  rshape.setAttribute('id', 'zoom_rshape');
  for(var i=0; i<rshape.childNodes.length; i++) {
    rshape.childNodes[i].removeAttribute('id');
  }
  var fcw = this.v.now.tform.width * this.v.zoom.scale;
  var fch = this.v.now.tform.height * this.v.zoom.scale;
  var vbox = [0, 0, this.v.now.tform.width, this.v.now.tform.height];
  rshape.setAttribute('viewBox', vbox.join(' '));
  rshape.setAttribute('width', fcw);
  rshape.setAttribute('height', fch);

  rshape.removeAttribute('style');
  style = [];
  style.push('position: absolute');
  var scaled_img_left = this.v.zoom.sizeby2 - px * this.v.zoom.scale;
  var scaled_img_top  = this.v.zoom.sizeby2 - py * this.v.zoom.scale;
  style.push('top:' + scaled_img_top + 'px');
  style.push('left:' + scaled_img_left + 'px');
  rshape.setAttribute('style', style.join(';'));

  if ( this.v.zoom.container.childNodes.length === 0 ) {
    // first time: add both filecontent and region shapes
    this.v.zoom.container.appendChild(this.v.zoom.filecontent);
    this.v.zoom.container.appendChild(rshape);
  } else {
    // next time: just update region shapes
    this.v.zoom.container.replaceChild(rshape, this.v.zoom.container.childNodes[1]);
  }
}


///
/// Region select/unslect handler
///
_via_ctrl.prototype.region_select_next = function() {

}

_via_ctrl.prototype.region_select_prev = function() {

}

_via_ctrl.prototype.region_select_toggle = function(rid) {
  if ( this.v.now.region_select.rid_list.includes(rid) ) {
    this.region_unselect(rid);
  } else {
    this.region_select(rid);
  }
}

_via_ctrl.prototype.region_select = function(rid) {
  var new_svg_element = this.v.layers.rshape.getElementById(rid);
  this.svg_apply_theme( new_svg_element, this.v.settings.theme.svg.SELECTED_REGION );
  this.v.now.region_select.rid_list.push(rid);
}

_via_ctrl.prototype.region_select_all = function() {
  this.region_select_n( this.v.now.all_rid_list );
  if ( this.v.now.region_select.rid_list.length ) {
    this.v.set_state( this.v.state.REGION_SELECTED );
  }
}

_via_ctrl.prototype.region_select_n = function(rid_list) {
  var n = rid_list.length;
  for ( var i=0; i<n; i++ ) {
    this.region_select( rid_list[i] );
  }
}

_via_ctrl.prototype.region_unselect = function(rid) {
  var index = this.v.now.region_select.rid_list.indexOf(rid);
  this.v.now.region_select.rid_list.splice(index, 1);

  var old_svg_element = this.v.layers.rshape.getElementById( rid );
  this.svg_apply_theme( old_svg_element, this.v.settings.theme.svg.REGION );
}

_via_ctrl.prototype.region_unselect_all = function() {
  var n = this.v.now.region_select.rid_list.length;
  for ( var i=0; i<n; i++ ) {
    var rid = this.v.now.region_select.rid_list[i];
    var old_svg_element = this.v.layers.rshape.getElementById( rid );
    this.svg_apply_theme( old_svg_element, this.v.settings.theme.svg.REGION );
  }
  this.v.now.region_select.rid_list = [];
  this.v.now.region_select.fileid = '';
}

_via_ctrl.prototype.get_regions_inside_an_area = function(x0, y0, x1, y1) {
  var regions = [];
  var area = new _via_region(this.v.settings.REGION_SHAPE.RECT,
                             '',
                             [x0, y0, x1, y1], 1);

  var n = this.v.now.all_rid_list.length;
  var fileid = this.v.now.fileid;
  for ( var i=0; i<n; i++ ) {
    var rid = this.v.now.all_rid_list[i];
    var r = this.m.regions[fileid][rid];
    var nd = r.dview.length;

    for ( var j=0; j<nd; j += 2 ) {
      if ( area.is_inside( r.dview[j], r.dview[j+1] ) ) {
        regions.push(rid);
        break;
      }
    }
  }
  return regions;
}
