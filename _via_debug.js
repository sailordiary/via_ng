function _via_debug(via) {
  this.via = via;
  this.init();
}

_via_debug.prototype.init = function() {
  this.via.c.show_message("Loading debug files ...", 0);
  var allp = [];
  allp.push( this.via.m.add_file_from_base64("unit_test_2.jpg", _via_unit_test_img[2]) );
  allp.push( this.via.m.add_file_from_base64("unit_test_1.jpg", _via_unit_test_img[1]) );
  allp.push( this.via.m.add_file_from_base64("unit_test_0.jpg", _via_unit_test_img[0]) );

  var ok = 0;
  var err = 0;

  /*
  for ( var i = 0; i < _via_test_filelist.length; i++ ) {
    var url  = _via_test_filelist[i];
    var type = this.url_filetype(url);
    if ( type !== "unknown" ) {
      allp.push( this.m.add_file_from_url(url, type) );
    } else {
      err++;
    }
  }
  */
  Promise.all( allp ).then( function(result) {
    for ( var i=0; i<result.length; i++ ) {
      if ( result[i].startsWith("Error") ) {
        err++;
      } else {
        ok++;
      }
    }
    var message = "Loaded " + ok + " files";
    if ( ok ) {
      this.via.c.load_file_from_index(1);
    }

    if ( err ) {
      message += " ( error loading " + err + " URL )";
    }
    this.via.c.show_message(message);
  }.bind(this));

  setTimeout( function() {
    var polygon1 = [280, 329,179, 279,189, 357,311, 455,530, 473,662, 450,684, 272,626, 222,538, 253,535, 336,403, 306];
    var polygon2 = [55, 453,64, 596,293, 662,715, 561,950, 668,1023, 526,781, 477,389, 520,241, 544];
    var polygon3 = [841, 122,781, 253,931, 305,922, 34,339, 28,507, 134,728, 81];
    var rect1    = [50, 50, 300, 300];
    var rect1a   = [100, 100, 250, 250];
    var rect1b   = [400, 100, 450, 150];
    var rect1c   = [600, 100, 675, 250];
    var circle1  = [250, 200, 300, 270];
    var ellipse1 = [400, 400, 450, 500];
    var line1    = [500, 500, 600, 600];
    var polyline1= [200, 200, 250,250, 200,300, 40,50];

    var fileid  = this.via.v.now.fileid;
    var tform_scale = this.via.v.now.tform.scale;
/*
    this.via.c.set_region_shape('rect');
    this.via.v.nvertex = rect1;
    this.via.c.add_region_from_nvertex();
    this.via.v.nvertex = rect1a;
    this.via.c.add_region_from_nvertex();
    this.via.v.nvertex = rect1b;
    this.via.c.add_region_from_nvertex();
    this.via.v.nvertex = rect1c;
    this.via.c.add_region_from_nvertex();

    this.via.c.set_region_shape('circle');
    this.via.v.nvertex = circle1;
    this.via.c.add_region_from_nvertex();
*/
    this.via.v.nvertex = [];
  }.bind(this), 1000);
}
