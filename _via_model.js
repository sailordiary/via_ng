
////////////////////////////////////////////////////////////////////////////////
//
// @file        _via_model.js
// @description Implements Model in the MVC architecture of VIA
// @author      Abhishek Dutta <adutta@robots.ox.ac.uk>
// @date        17 June 2017
//
////////////////////////////////////////////////////////////////////////////////

function _via_model() {
  this.about = {};
  this.about.name = 'VGG Image Annotator';
  this.about.shortname = 'VIA';
  this.about.version = '2.0.0';

  this.regions = {};

  this.repo = {};
  this.repo.local = {};
  this.repo.remote = {};

  this.files = {};
  this.files.metadata = {};
  this.files.content = {};
  this.files.fileid_list = [];
  this.files.file_index_list = {};
}

_via_model.prototype.init = function( via_ctrl ) {
  //console.log('Initializing _via_model ...');
  this.c = via_ctrl;
}

///
/// add/remove files being annotated
///
_via_model.prototype.add_file_from_base64 = function(filename, data_base64) {
  var filename   = filename;
  var filesize   = data_base64.length;
  var frame      = 0;
  var count      = 1;
  var filesource = 'base64';
  var filetype   = data_base64.substr(5, 5);
  var filecontent= data_base64;

  return new Promise( function(ok_callback, err_callback) {
    this.add_file(filename, filesize, frame, count, filesource, filetype, filecontent).then( function(fileid) {
      ok_callback(fileid);
    }, function(error_msg) {
      err_callback(error_msg);
    });
  }.bind(this));
}

_via_model.prototype.add_file_from_url = function(url, type) {
  var filename   = url;
  var filesize   = -1;
  var frame      = 0;
  var count      = 1;
  var filesource = 'url';
  var filetype   = type;
  var filecontent= url;

  return new Promise( function(ok_callback, err_callback) {
    this.add_file(filename, filesize, frame, count, filesource, filetype, filecontent).then( function(fileid) {
      ok_callback(fileid);
    }.bind(this), function(error_msg) {
      err_callback(error_msg);
    }.bind(this));
  }.bind(this));
}

_via_model.prototype.add_file = function(filename, filesize, frame, count, filesource, filetype, filecontent) {
  // filename    : local filename or image url
  // filesize    : file size in bytes (-1 when filesource is url)
  // frame       : frame index in video (0 for image)
  // count       : number of frames starting from from 'frame' (1 for image)
  // filetype    : {image, video} (0 for unknown)
  // filesource  : {local, url, base64}
  // filecontent : local file reference, url, base64 data
  //
  return new Promise( function(ok_callback, err_callback) {
    this.c.file_hash(filename, filesize, frame, count).then( function(fileid) {
      if ( this.files.fileid_list.includes(fileid) ) {
        this.c.show_message('File ' + filename + ' already loaded. Skipping!');
	err_callback('Error : file already loaded : ' + filename);
      }
      this.files.file_index_list[fileid] = this.files.fileid_list.length;
      this.files.fileid_list.push(fileid);

      this.files.content[fileid] = filecontent;

      this.files.metadata[fileid] = {};
      this.files.metadata[fileid].fileid   = fileid;
      this.files.metadata[fileid].filename = filename;
      this.files.metadata[fileid].filesize = filesize;
      this.files.metadata[fileid].frame    = frame;
      this.files.metadata[fileid].count    = count;
      this.files.metadata[fileid].source   = filesource
      this.files.metadata[fileid].type     = filetype;

      this.regions[fileid] = {};
      ok_callback(fileid);
    }.bind(this), function(error) {
      err_callback('Error : error computing unique hash for file : '
                   + filename + '{error: ' + error + '}');
    }.bind(this));
  }.bind(this));
}

_via_model.prototype.add_file_local = function(file) {
  var filename   = file.name;
  var filesize   = file.size;
  var frame      = 0;
  var count      = 1;
  var filesource = 'local';
  var filetype   = file.type.substr(0, 5);
  var filecontent= file

  return new Promise( function(ok_callback, err_callback) {
    this.add_file(filename, filesize, frame, count, filesource, filetype, filecontent).then( function(fileid) {
      ok_callback(fileid);
    }, function(error_msg) {
      err_callback(error_msg);
    });
  }.bind(this));
}

///
/// Region management (add/update/delete/...)
///
_via_model.prototype.region_add = function(fileid, tform_scale, shape, nvertex) {
  // unique region id is constructed as follows:
  //   shape + Date.now() + first_vertex
  // where,
  // shape : {rect, circle, ellipse, line, polyline, polygon, point}
  // Date.now() : number of milliseconds elapsed since epoch
  // first_vertex : 3 digit coordinate padded with 0
  return new Promise( function( ok_callback, err_callback ) {
    //var id   = this.m.settings.useremail + '_' + Date.now() + '_' + name;
    var rid   = shape + '_' + Date.now() + '_' + ('000' + nvertex[0]).slice(-3);
    var region = new _via_region(shape, rid, nvertex, tform_scale);
    this.regions[fileid][rid] = region;

    ok_callback(rid);
  }.bind(this) );
}

_via_model.prototype.region_del = function() {
}

_via_model.prototype.region_move = function(fileid, rid, dx, dy) {
  return new Promise( function( ok_callback, err_callback ) {
    this.regions[fileid][rid].move( dx, dy );
    ok_callback( {fileid: fileid, region_id: rid} );
  }.bind(this) );
}

_via_model.prototype.region_resize = function(fileid, rid, vertex, x_new, y_new) {
  return new Promise( function( ok_callback, err_callback ) {
    this.regions[fileid][rid].resize( vertex, x_new, y_new );
    ok_callback( {fileid: fileid, region_id: rid} );
  }.bind(this) );
}

///
/// Region query
///
_via_model.prototype.is_point_in_a_region = function(fileid, px, py) {
  // returns region_id if (px,py) is inside a region
  // otherwise returns ''

  var rid_list = [];
  for( var rid in this.regions[fileid] ) {
    if ( this.regions[fileid].hasOwnProperty(rid) ) {
      if ( this.regions[fileid][rid].is_inside( px, py ) ) {
        rid_list.push( rid );
      }
    }
  }

  switch( rid_list.length ) {
  case 0:
    return '';
    break;
  case 1:
    return rid_list[0];
    break;
  default:
    return this.region_nearest_to_point(px, py, rid_list, fileid);
    break;
  }

  // @todo : for large number of polygons, employ more efficient algorithms
  // using the Slab Decomposition algorithm
  // ref: https://en.wikipedia.org/wiki/Point_location#Slab_decomposition
}

_via_model.prototype.region_nearest_to_point = function(px, py, rid_list, fileid) {
  var min_edge_distance = Number.MAX_VALUE;
  var min_edge_index = -1;
  for ( var i=0; i<rid_list.length; i++ ) {
    var rid = rid_list[i];
    var edge_distance = this.regions[fileid][rid].dist_to_nearest_edge(px,py);
    if ( edge_distance < min_edge_distance ) {
      min_edge_distance = edge_distance;
      min_edge_index = i;
    }
  }
  return rid_list[ min_edge_index ];
}

_via_model.prototype.is_point_inside_region = function(fileid, rid, px, py) {
  return this.regions[fileid][rid].is_inside( px, py );
}

// returns edge-id if (px,py) is on a selected region edge
// otherwise -1
_via_model.prototype.is_on_region_edge = function(fileid, rid, px, py, tolerance) {
  return this.regions[fileid][rid].is_on_edge( px, py, tolerance );
}

_via_model.prototype.is_on_these_region_edge = function(fileid, rid_list, px, py, tolerance) {
  var n = rid_list.length;
  for ( var i=0; i<n; i++ ) {
    var rid  = rid_list[i];
    var edge = this.regions[fileid][rid].is_on_edge( px, py, tolerance );
    if ( edge !== -1 ) {
      return {'rid':rid, 'id':edge};
    }
  }
  return {'rid':-1, 'id':-1};
}

_via_model.prototype.is_point_inside_these_regions = function(fileid, rid_list, px, py) {
  var n = rid_list.length;
  var candidate_rid_list = [];

  for ( var i=0; i<n; i++ ) {
    var rid = rid_list[i];
    if ( this.regions[fileid][rid].is_inside( px, py ) ) {
      candidate_rid_list.push( rid );
    }
  }

  switch( candidate_rid_list.length ) {
  case 0:
    return '';
    break;
  case 1:
    return candidate_rid_list[0];
    break;
  default:
    return this.region_nearest_to_point(px, py, candidate_rid_list, fileid);
    break;
  }
}

///
/// metadata
///
