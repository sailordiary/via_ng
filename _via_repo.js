function _via_repo(unique_state_hash) {
  this.api_baseurl  = "";
  this.access_token = "";
  this.id           = "";
  this.name         = "";
  this.state_hash   = unique_state_hash;

  _via_repo_gitlab.call(this);
}

function _via_repo_gitlab() {
  this.api_baseurl  = "http://gitlab.com/api/v4";
  this.oauth_url    = "https://gitlab.com/oauth/authorize";
  this.appid        = "56d3acfb2921714b4314fb26357354d637270e5b0fd95236145d5fed22a6f2e4";
  this.secret       = "e4c8eedf7257d3d96c1a8080d3e1156dfa49be7c43cb9ac67ed82b3c66eeb0f3";

  this.authenticate = _via_repo_gitlab.prototype.authenticate;

  this.init         = _via_repo_gitlab.prototype.init;
  this.clone        = _via_repo_gitlab.prototype.clone;
  this.add          = _via_repo_gitlab.prototype.add;
  this.commit       = _via_repo_gitlab.prototype.commit;

  this.push         = _via_repo_gitlab.prototype.push;

  this.pull         = _via_repo_gitlab.prototype.pull;
  this.fetch        = _via_repo_gitlab.prototype.fetch;
  this.merge        = _via_repo_gitlab.prototype.merge;
}

_via_repo_gitlab.prototype.authenticate = function() {
  var args = [];
  args.push('client_id=' + this.appid);
  args.push('redirect_uri=http://localhost/');
  args.push('response_type=token');
  args.push('state=' + this.state_hash);

  var url = this.oauth_url + '?' + args.join('&');
  var win = window.open(url, '_blank');
  win.focus();

  /*
  return new Promise( function(ok_callback, err_callback) {
    var gs = new XMLHttpRequest();
    gs.addEventListener('error', err_callback);
    gs.addEventListener("abort", err_callback);
    gs.addEventListener("load", function() {
      var response = this.responseText;
      console.log(response);
    });

    var args = [];
    args.push('client_id=' + this.appid);
    args.push('redirect_uri=http://localhost');
    args.push('response_type=token');
    args.push('state=' + this.state_hash);

    var url = this.oauth_url + '?' + args.join('&');
    gs.open( 'GET', url );
    gs.setRequestHeader( 'Access-Control-Allow-Origin', '1' );
    gs.send();
  }.bind(this));
  */
}

_via_repo_gitlab.prototype.init = function() {
}

_via_repo_gitlab.prototype.clone = function() {
}

_via_repo_gitlab.prototype.add = function() {
}

_via_repo_gitlab.prototype.commit = function() {
}

_via_repo_gitlab.prototype.push = function() {
}

_via_repo_gitlab.prototype.pull = function() {
}

_via_repo_gitlab.prototype.fetch = function() {
}

_via_repo_gitlab.prototype.merge = function() {
}
