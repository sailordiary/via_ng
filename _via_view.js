
////////////////////////////////////////////////////////////////////////////////
//
// @file        _via_view.js
// @description Implements View in the MVC architecture of VIA
// @author      Abhishek Dutta <adutta@robots.ox.ac.uk>
// @date        17 June 2017
//
////////////////////////////////////////////////////////////////////////////////


function _via_view() {

  this.state = {UNKNOWN: 'UNKNOWN',
                IDLE: 'IDLE',
                REGION_SELECTED: 'REGION_SELECTED',
                REGION_SELECT_OR_DRAW_POSSIBLE: 'REGION_SELECT_OR_DRAW_POSSIBLE',
                SELECT_ALL_INSIDE_AN_AREA_ONGOING: 'SELECT_ALL_INSIDE_AN_AREA_ONGOING',
                REGION_UNSELECT_ONGOING: 'REGION_UNSELECT_ONGOING',
                REGION_SELECT_TOGGLE_ONGOING: 'REGION_SELECT_TOGGLE_ONGOING',
                REGION_MOVE_ONGOING: 'REGION_MOVE_ONGOING',
                REGION_RESIZE_ONGOING: 'REGION_RESIZE_ONGOING',
                REGION_DRAW_ONGOING: 'REGION_DRAW_ONGOING',
                REGION_DRAW_NCLICK_ONGOING: 'REGION_DRAW_NCLICK_ONGOING',
                FILE_LOAD_ONGOING: 'FILE_LOAD_ONGOING'
               };
  this.state_now = this.state.UNKNOWN;

  this.layers = {}; // content layers in this.v.view_panel

  // record of the immediate past events
  this.last = {};
  this.last.mousemove = {};
  this.last.mousedown = {};
  this.last.mouseup   = {};
  this.nvertex = []; // array of _via_point

  this.now = {};
  // these fileds are populated during runtime:
  // this.now.fileid
  // this.now.tform.{x,y,width,height,scale,content_width, content_height}
  // this.now.{all_rid_list,polygon_rid_list,other_rid_list}
  // this.now.meta_keypress.{shiftkey, ctrlkey, altkey}
  // this.now.region_select.rid_list

  // User views a scaled version of original image (that fits in the web
  // browser display panel). The tform parameters are needed to convert
  // the user drawn regions in the web browser to coordinates in the
  // original image dimension
  // For example: original_img_x = drawn_x * tform.scale;
  this.now.tform = {};

  this.now.region_shape = '';
  this.now.region_select = {};
  this.now.region_select.rid_list = [];
  this.now.region_select.fileid = '';

  this.now.view_tmp_region = {};
  this.now.view_tmp_region.rid_list = [];
  this.now.view_tmp_region.DEFAULT_RID = '_via_tmp_svg_region';
  this.now.view_tmp_region.RID_PREFIX_MOVE = '_via_tmp_move_';
  this.now.view_tmp_region.RID_PREFIX_RESIZE = '_via_tmp_resize_';


  this.now.all_rid_list = [];
  this.now.polygon_rid_list = [];
  this.now.other_rid_list = [];

  this.now.meta_keypress = {};
  this.now.meta_keypress.shiftkey = false;
  this.now.meta_keypress.altkey = false;
  this.now.meta_keypress.ctrlkey = false;

  this.settings = {};
  this.settings.username = '_via_user_name';
  this.settings.useremail= '_via_user_email';
  this.settings.REGION_SHAPE =  {RECT    :'rect',
                                 CIRCLE  :'circle',
                                 ELLIPSE :'ellipse',
                                 POINT   :'point',
			         LINE    :'line',
                                 POLYGON :'polygon',
			         POLYLINE:'polyline'};

  this.settings.theme    = {};
  this.settings.theme.svg= {};
  this.settings.theme.svg.REGION = { 'fill': 'black',
                                     'fill-opacity': '0.05',
                                     'stroke': 'yellow',
                                     'stroke-width': '2'
                                   };
  this.settings.theme.svg.SELECTED_REGION = { 'fill': 'white',
                                              'fill-opacity': '0.2',
                                              'stroke': 'black',
                                              'stroke-width': '2'
                                            };
  this.settings.theme.svg.ON_DRAWING = { 'fill': 'none',
                                         'fill-opacity': '0',
                                         'stroke': 'red',
                                         'stroke-width': '2'
                                       };
  this.settings.theme.svg.ON_MOVE = { 'fill': 'white',
                                      'fill-opacity': '0.2',
                                      'stroke': 'red',
                                      'stroke-width': '2'
                                    };

  this.settings.theme.svg.ON_SELECT_AREA_DRAW = { 'fill': 'none',
                                                  'stroke': 'red',
                                                  'stroke-width': '2',
                                                  'stroke-dasharray': '5,5'
                                                };
  // point is a circle with radius 1, the radius is set higher for visualization
  this.settings.theme.svg.POINT_SHAPE_DRAW_RADIUS = 2;

  this.settings.theme.MSG_TIMEOUT_MS = 4000;
  this.settings.theme.MOUSE_CLICK_TOLERANCE2    = 4; // 2 pixels
  this.settings.theme.REGION_ON_EDGE_TOLERANCE2 = 4; // 2 pixels
  this.settings.theme.THETA_TOLERANCE           = Math.PI / 18; // 10 degrees
  this.settings.theme.MOUSE_VERTEX_TOLERANCE = 10; // pixels
  this.settings.theme.FILE_VIEW_SCALE_MODE = 'fit'; // {original, fit}

  // zoom
  this.zoom = {};
  this.zoom.container = {};
  this.zoom.container_id = 'zoom_panel';
  this.zoom.filecontent = {};

  this.zoom.is_enabled = false;
  this.zoom.size = 300; // a square region with size in pixels
  this.zoom.sizeby2 = this.zoom.size/2;
  this.zoom.scale = 2.0; // 1.0 for no scale
  this.zoom.DEFAULT_SCALE = 2.0;

  //this.local_file_selector : created by init_local_file_selector()

  // notify _via_ctrl if user interaction events
  this.handleEvent = function(e) {
    switch(e.currentTarget.id) {
    case 'rect':
    case 'circle':
    case 'ellipse':
    case 'polygon':
    case 'line':
    case 'polyline':
    case 'point':
      this.c.set_region_shape(e.currentTarget.id);
      break;

    case 'add_file_local':
    case 'menubar_add_file_local':
      this.c.select_local_files();
      break;

    case 'move_to_prev_file':
      this.c.load_prev_file();
      break;
    case 'move_to_next_file':
      this.c.load_next_file();
      break;

    case 'zoom_in':
      this.c.zoom_activate();
      break;
    case 'zoom_reset':
      this.c.zoom_deactivate();
      break;

    case 'region_delete_selected':
      this.c.delete_selected_regions();
      break;

    case 'region_select_all':
      this.c.region_select_all();
      break;

    default:
      console.log('_via_view: handler unknown for event: ' + e.currentTarget);
    }
    e.stopPropagation();
    //this.c.layers['top'].focus();
  }
}


///
/// state maintainance
///
_via_view.prototype.set_state = function(state) {
  if ( this.state.hasOwnProperty( state ) ) {
    this.state_now = state;
    //console.log('State = ' + this.state_now);
  } else {
    console.log('_via_view.prototype.set_state() :: Cannot set to unknown state: ' + state);
  }
}

_via_view.prototype.init = function( via_ctrl, view_panel, message_panel ) {
  //console.log('Initializing _via_view ...');
  this.c = via_ctrl;

  if( typeof(view_panel) === 'undefined' ) {
    console.log('Error: _via_view.prototype.init() : view panel has not been defined');
  } else {
    this.view_panel = view_panel;
  }

  if( typeof(message_panel) === 'undefined' ) {
    _via_ctrl.prototype.show_message = function(msg, t){};
  } else {
    this.message_panel = message_panel;
  }

  this.view_panel.innerHTML = '';

  this.init_local_file_selector();
  this.register_ui_action_handlers();

  //this.init_metadata_io_panel_drag_handler();
}

_via_view.prototype.init_local_file_selector = function() {
  this.local_file_selector = document.createElement('input');
  this.local_file_selector.setAttribute('id', 'local_file_selector');
  this.local_file_selector.setAttribute('type', 'file');
  this.local_file_selector.setAttribute('name', 'files[]');
  this.local_file_selector.setAttribute('multiple', 'multiple');
  this.local_file_selector.classList.add('display-none');
  this.local_file_selector.setAttribute('accept', '.jpg,.jpeg,.png,.bmp,.mp4,.ogg,.webm');
  this.local_file_selector.addEventListener('change',
                                            this.c.add_user_sel_local_files.bind(this.c),
                                            false);

  this.view_panel.appendChild(this.local_file_selector);
}

_via_view.prototype.register_ui_action_handlers = function() {
  // defined in javascript embedded in file _via_view.html
}
