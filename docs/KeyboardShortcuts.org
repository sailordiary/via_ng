| Key Combination     | Description                           |
|---------------------+---------------------------------------|
| Shift + Click       | select multiple regions               |
| Arrow keys          | move selected regions by 1 pixel      |
| Shift + Arrow keys  | move selected regions by 10 pixels    |
| Shift + Region draw | select all regions inside drawn regin |
| Ctrl + a            | select all regions                    |
| Ctrl + c            | copy selected regions                 |
| Ctrl + v            | paste copied regions                  |
|                     |                                       |
|---------------------+---------------------------------------|
