# VGG Image Annotator 2 (VIA2)
VGG Image Annotator version 2.0.0 (VIA2) will contain the following new features 
while retaining all the features of VIA 1.0.0 :
 * [Video Annotation](#video-annotation)
 * [Distributed Annotation](#distributed-annotation)
 * [Assistant](#assistant)
 * [User Interface](#user-interface)
 * [File List](#file-list)
 * [Import and Export](#import-and-export)
 * [Code Documentation](#code-documentation)
 
VIA2 can be used as: (a) a standalone image and video annotator; (b) a library 
in other projects.

You may also want to review the following [References](#references).

## Video Annoation
 * annotation of video based on shots (t, t + delta)
 * all actions possible on images will be made available for video

## Distributed Annotation
 * Project: contains the full state of a VIA2 session (files, annotations, settings, etc)
 * Load and save projects
 * Push (or pull) projects to (or from) any git repository (github, gitlab, etc)
 * maintain a local git repository for offline users

## Assistant
 * Automatically annotate an image (on user's request)
 * Uses computer vision algorithms to detect objects, text, etc. in an image
 * Should be compatible with standard vision api (VGG Vision API, Google Vision API, etc)

## User Interface
 * follow Google's material design rules for UX
 * settings page for all configurations (save settings to localStorage)
 * When is doubt, see how [blender](https://www.blender.org/) did it.

## File List
 * Compact grid based image list (showing image previews, metadata, etc)
 * Drag and drop based image addition and deletion
 * Search image list using regular expression on metadata, filename, etc
 * Bulk update of file attributes for selected files

## Import and Export
 * Import image urls from a website 
 * Import from a file containing list of image url
 * Drag and drop media content or url
 * Create a standalone version of a file and its annotations (HTML + SVG + JS)

## Code Documentation
VIA2 is an open source project and we welcome contributions from new developers.
We have developed extensive code documentation and VIA2 design documentation to 
help new developers understand the codebase.

### Design
 * [VIA2 code based on Model, View, Controller design pattern](docs/code_documentation/via_mvc_design.png)
 * [Layered structure of VIA2 interface](docs/code_documentation/via_layers.png)
 * [Data flow](docs/code_documentation/via_data_flow.png)
 * [User interface interactions](docs/code_documentation/user_interface_interaction.png)
 * [Project interactions](docs/code_documentation/via_project_interaction.png)

### Coding Convention
 * VIA2 codebase is based on Model, View, Controller (MVC) design pattern:
 * It uses several other design patterns for its components:
   * Observer
   * Memento
   * State
   * Iterator
   * Chain of Responsibility
   * ...



## References
 * Code Documentation
   * [Learning Javascript Design Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/)
   * [MVC Architecture - Google Chrome](https://developer.chrome.com/apps/app_frameworks)
   * [Model View Controller - Apple Developer](https://developer.apple.com/library/content/documentation/General/Conceptual/DevPedia-CocoaCore/MVC.html)
   * [W3C JavaScript best practices](https://www.w3.org/wiki/JavaScript_best_practices)
   * [Google Javascript style guide](https://google.github.io/styleguide/javascriptguide.xml)
 * User Interface
   * [Color Tool @ Material Design](https://material.io/color/)
   * [Matrial Design for Web](https://material.io/components/web/)
   * [Material Design](https://material.io/guidelines/material-design/introduction.html)
 * Distributed Annotation
   * [Git HTTP transport protocol documentation](https://gist.github.com/schacon/6092633)
 * Assistant
   * [TensorFlow Object Detection API](https://opensource.googleblog.com/2017/06/supercharge-your-computer-vision-models.html)
     * [Code](https://github.com/tensorflow/models/tree/master/object_detection)
     * [Paper](https://arxiv.org/abs/1611.10012)
   * [Google Cloud Vision API](https://cloud.google.com/vision/docs/)
     * [Detecting labels](https://cloud.google.com/vision/docs/detecting-labels)  
     * [Text Detection](https://cloud.google.com/vision/docs/detecting-text)

  
Abhishek Dutta  
21 June 2017
