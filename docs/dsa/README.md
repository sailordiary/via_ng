# Data Structure and Algorithms in VIA2

VIA2 implements a large number of algorithms from [computational geometry](https://en.wikipedia.org/wiki/Computational_geometry) 
and efficient data structures to record and analyse user defined regions on images and videos.

If an image has few regions (say, 10), then it is **not** computationally 
intensive to check if any *mousemove* or *mouseup/down* event occured inside 
one of these regions. However, we have seen uses cases (@Eli Walker's project 
involved annotation of an image that contained ~100 rectangular regions. Such 
high density of regions demand an efficient data structure which can run basic 
query operations (is a given point inside one of the regions?) efficiently.

## Fundamental Operations Needed in VIA
 1. Is a point inside any of the N regions?
   * We want to change the mouse icon from default to pointer when a user hovers 
mouse over a selected region.
   * Region move, select/unselect operations
 2. Which of the N regions contains a given point?
   * to select/unselect (toggle operation) a region

 3. Is a point on the edge of any of the N regions?
   * we change the mouse cursor type from default to resize (arrow cursor) to indicate the region resize is possible as boundary
 4. Which edge vertex is a given point closest to?
   * to allow resize of a region boundary defined by a set of vertices
 5. Is a point inside more than one region?
 6. Is a region degenerate?
 7. Select all the regions (or region vertices) that lie in a user drawn box?

## Query: is a point (px,py) inside one of the regions?
We can have regions of different shapes. Some of these regions can be represented 
by a parameteric equation (rectangle, circle, ellipse). Some shapes (point, 
line, polyline, etc) do not contain any area and therefore for such shapes this 
query will always return false. 

Polygon is the only region shape that requires deeper analysis to answer this 
query. To answer this query for polygons, we can use the following algorithms:
 * if number of polygon is less, we apply the [Winding number algorithm](https://en.wikipedia.org/wiki/Point_in_polygon#Winding_number_algorithm) to each polygon sequentially
 * we apply the [Slab decomposition](https://en.wikipedia.org/wiki/Point_location#Slab_decomposition) 
 * a more generic solution is to use [k-d tree](https://en.wikipedia.org/wiki/K-d_tree). 
See [Skiena 1998, page 389]

## Query: is a point (px,py) on the edge of one of the selected regions?
To simplify this problem, we constrain this query to only one of the selected 
regions and reformualte the query as follows: is point (px,py) on the edge of a 
given region?

## References
 * [The Computational Geometry Algorithms Library](http://www.cgal.org/)
 * [Advanced Data Structures](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-851-advanced-data-structures-spring-2012/calendar-and-notes/)
 * [Point Location](https://en.wikipedia.org/wiki/Point_location)
 * Skiena, Steven S. The algorithm design manual: Text. Vol. 1. Springer Science & Business Media, 1998.

Abhishek Dutta  
16 July 2017
