#!/usr/bin/env sh

VIA_JS_FILE=via.js
VIA_CSS_FILE=via.css
TEMPLATE_HTML_FILE=index.html
TARGET_HTML_FILE=via2.html
GOOGLE_ANALYTICS_JS_FILE=via_google_analytics.js

TMP_FILE1=temp1.html
TMP_FILE2=temp2.html

# source: http://stackoverflow.com/questions/16811173/bash-inserting-one-files-content-into-another-file-after-the-pattern
sed -e '/<!--AUTO_INSERT_VIA_JS_HERE-->/r./'$VIA_JS_FILE $TEMPLATE_HTML_FILE > $TMP_FILE1
sed -e '/<!--AUTO_INSERT_GOOGLE_ANALYTICS_JS_HERE-->/r./'$GOOGLE_ANALYTICS_JS_FILE $TMP_FILE1 > $TMP_FILE2
sed -e '/<!--AUTO_INSERT_VIA_CSS_HERE-->/r./'$VIA_CSS_FILE $TMP_FILE2 > $TARGET_HTML_FILE

rm -f $TMP_FILE1 $TMP_FILE2

echo 'Written html file to '$TARGET_HTML_FILE
